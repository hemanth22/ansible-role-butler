Ansible Role: Butler
====================

Butler for jenkins to import/export plugins, jobs and credentials.

[![Build Status](https://travis-ci.org/hemanth22/ansible-role-butler.svg?branch=master)](https://travis-ci.org/hemanth22/ansible-role-butler)

Good Tutorial link: http://www.blog.labouardy.com/butler-cli-import-export-jenkins-plugins-jobs/

Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      remote_user: vagrant
      roles:
         - role: hemanth22.butler

License
-------

GNU General Public License v3.0

Author Information
------------------

This role was created in 2019 by Hemanth BITRA.
